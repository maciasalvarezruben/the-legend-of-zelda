$(document).ready(function(){
  console.log("Welcome to the Game");

  var player = $("#player"); // Element player
  var scenario = $("#scenario"); // Element scenario
  var dx = 152; // Precisión parameters
  var dy = 200; // Precisión parameters

  captureKeys();

/* INICIO función para controlar al jugador */



  function bounce() {
    let w = 50;
    let h = 50;
    player.animate({ height: h + dy}, "slow");
    player.animate({ height: h - dy}, "fast");
    player.animate({ width: w + dy}, "slow");
    player.animate({ width: w - dy}, "fast");
  }



  function moveLeft() {
    player.css({left: player.position().left - dx});
    //player.animate({ left: player.position().left - dx}, "fast");
  }

  function moveRight() {
    player.css({left: player.position().left + dx});
    //player.animate({ left: player.position().left + dx}, "fast");

  }

  function moveDown() {
    player.css({top: player.position().top + dy});
    //player.animate({ top: player.position().top + dy}, "fast");
  }

  function moveUp() {
    player.css({top: player.position().top - dy});
    //player.animate({ top: player.position().top - dy}, "fast");
  }

  function jump() {
    player.css({top: player.position().top - 8*dy});
    // let startPoint = player.position();
    // player.animate({ top: startPoint.top - 8*dy}, "fast");
    // player.animate({ top: startPoint.top - 5*dy}, "fast");
    // player.animate({ top: startPoint.top - 3*dy}, "fast");
    // player.animate({ top: startPoint.top - dy}, "fast");
    // player.animate({ top: startPoint.top}, "fast");

  }

  function fire() {
    $('<div />').addClass("bullet").appendTo(player);
  }

/* FIN función para controlar al jugador */


/* INICIO función para capturar las teclas */
  function captureKeys() {
    $("body").keypress(function(){
      if (event.which == 97) {
        // A
        moveLeft();
      }

      if (event.which == 100) {
        // D
        moveRight();
      }

      if (event.which == 115) {
        // S
        moveDown();
      }

      if (event.which == 119) {
        // W
        moveUp();

      }

      if (event.which == 102) {
        // F
        fire();
      }

      if (event.which == 32) {
        // SPACE
    //    jump();
          bounce();
      }


      $( "#log" ).prepend( event.type + ": " +  event.which + "<br/>");

      return false;

    });
  }
/* FIN función para capturar las teclas */





});

// https://stackoverflow.com/questions/7298507/move-element-with-keypress-multiple
/* const Player = {
  el: document.getElementById('player'),
  x: 0,
  y: 0,
  speed: 2,
  move() {
    this.el.style.transform = `translate(${this.x}px, ${this.y}px)`;
  }
};

const K = {
  fn(ev) {
    const k = ev.which;
    if (k >= 37 && k <= 40) {
      ev.preventDefault();
      K[k] = ev.type === "keydown"; // If is arrow
    }
  }
};

const update = () => {
  let dist = K[38] && (K[37] || K[39]) || K[40] && (K[37] || K[39]) ? 0.707 : 1;
  dist *= Player.speed;
  if (K[37]) Player.x -= dist;
  if (K[38]) Player.y -= dist;
  if (K[39]) Player.x += dist;
  if (K[40]) Player.y += dist;
  Player.move();
}

document.addEventListener('keydown', K.fn);
document.addEventListener('keyup', K.fn);

(function engine() {
  update();
  window.requestAnimationFrame(engine);
}());
*/
